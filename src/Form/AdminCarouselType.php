<?php

namespace App\Form;

use App\Entity\CarouselPicture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AdminCarouselType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('photo1',FileType::class)
            ->add('photo2',FileType::class)
            ->add('photo3',FileType::class)
            ->add('photo4',FileType::class)
            ->add('photo5',FileType::class)
            ->add('photo6',FileType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CarouselPicture::class,
        ]);
    }
}
