<?php

namespace App\Controller;

use App\Entity\TexteAccueil;
use App\Form\EcoleTexteType;
use App\Form\GestionTexteType;
use App\Entity\TextePresentationEcole;
use App\Repository\TexteAccueilRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TextePresentationEcoleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/admin/texte")
 * 
 */
 
class AdminTexteController extends AbstractController
{
     /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    /**
     * @Route("/texte", name="admin_texte")
     * 
     */
    public function indexGestionTexte(Request $request)
    {
        $texteAccueil = new TexteAccueil();

        $formAccueil = $this->createForm(GestionTexteType::class, $texteAccueil);
        $formAccueil->handleRequest($request);
      
        if ($formAccueil->isSubmitted() && $formAccueil->isValid()) {
            $file1 = $texteAccueil->getPhoto();
            $fileName1 = $this->generateUniqueFileName().'.'.$file1->guessExtension();

            try {
                $file1->move($this->getParameter('photoSection_directory'),
                $fileName1);
            }

            catch (FileException $e) {

            }

            $texteAccueil->setPhoto($fileName1);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($texteAccueil);
            $entityManager->flush();
            return $this->redirectToRoute('admin_texte');
        }
    

        $texteEcole = new TextePresentationEcole();

        $formEcole = $this->createForm(EcoleTexteType::class, $texteEcole);
        $formEcole->handleRequest($request);

        if ($formEcole->isSubmitted() && $formEcole->isValid()) {
            $file2 = $texteEcole->getPhoto();
            $fileName2 = $this->generateUniqueFileName().'.'.$file2->guessExtension();
             
            try {
                $file2->move($this->getParameter('photoSection_directory'),
                $fileName2);
            }

            catch (FileException $e) {

            }
            $texteEcole->setPhoto($fileName2);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($texteEcole);
            $entityManager->flush();

            return $this->redirectToRoute('admin_texte');
        }

        return $this->render('admin_texte/ajouter_section.html.twig', [
            'formAccueil' => $formAccueil->createView(),
            'formEcole' => $formEcole->createView()
        ]);
    }

     /**
     * @Route("/show/accueil", name="admin_texte_show_accueil")
     * 
     */
    public function showTexteAccueil(Request $request, TexteAccueilRepository $texteAccueilRepo)
    {

      $sectionsAccueil = $texteAccueilRepo->findAll();

      return $this->render('admin_texte/showAccueil.html.twig', [
      'sectionsAccueil' => $sectionsAccueil
        ]);

    }
    
     /**
     * @Route("ecole/show", name="admin_texte_show_ecole")
     * 
     */
    public function showTexteEcole(Request $request, TextePresentationEcoleRepository $texteEcoleRepo)
    {

      $sectionsEcole = $texteEcoleRepo->findAll();

      return $this->render('admin_texte/showEcole.html.twig', [
      'sectionsEcole' => $sectionsEcole
        ]);

    }

    /**
     * @Route("ecole/delete/{id}", name="admin_texte_delete_ecole")
     * 
     */
    public function deleteTexteEcole($id,Request $request)
    {

       $photoEcoleTexte = $this->getDoctrine()->getRepository(TextePresentationEcole::class)->find($id);
         //Supprimer la photo dans le dossier
        $path = $this->getParameter('photoSection_directory');
        $fs = new Filesystem();
        $fs->remove($path.$photoEcoleTexte->getPhoto());
        //Supprimer la ligne dans la BDD
        $em = $this->getDoctrine()->getManager();
        $em->remove($photoEcoleTexte);
        $em->flush();
        return $this->redirectToRoute('admin_texte_show_ecole');

    }

      /**
     * @Route("accueil/delete/{id}", name="admin_texte_delete_accueil")
     * 
     */
    public function deleteTexteAccueil($id,Request $request)
    {

       $photoAccueilTexte = $this->getDoctrine()->getRepository(TexteAccueil::class)->find($id);
         //Supprimer la photo dans le dossier
        $path = $this->getParameter('photoSection_directory');
        $fs = new Filesystem();
        $fs->remove($path.$photoAccueilTexte->getPhoto());
        //Supprimer la ligne dans la BDD
        $em = $this->getDoctrine()->getManager();
        $em->remove($photoAccueilTexte);
        $em->flush();
        return $this->redirectToRoute('admin_texte_show_accueil');
        
    }

}
